"use strict";

// const name = "Regina";
// const url = `images/${name.toLowerCase()}.jpg`;
// const html = `<article class="pizzaThumbnail">
// <a href="${url}">
// <img src="${url}"/>
// <section>${name}</section>
// </a>
// </article>`;
var html = ""; // const data = ['Regina', 'Napolitaine', 'Spicy'];

var data = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}]; // for(let i = 0; i<data.length; i++) {
//     const name = data[i];
//     const url = `images/${name.toLowerCase()}.jpg`;
//     html += `<article class="pizzaThumbnail">
//     <a href="${url}">
//     <img src="${url}"/>
//     <section>${name}</section>
//     </a>
//     </article>`;
// }

data.sort(function (a, b) {
  return a.name > b.name;
});
data.sort(function (a, b) {
  return a.price_small > b.price_small;
});
data.sort(function (a, b) {
  if (a.price_small == b.price_small) {
    return a.price_large > b.price_large;
  } else {
    return a.price_small > b.price_small;
  }
});
var dataFilter1 = data.filter(function (e) {
  return e.base == 'tomate';
});
var dataFilter2 = data.filter(function (e) {
  return e.price_small < 6;
});
var dataFilter3 = data.filter(function (e) {
  return e.name.split("i") > 2;
});
dataFilter1.forEach(function (element) {
  html += "<article class=\"pizzaThumbnail\">\n    <a href=\"".concat(element.image, "\">\n    <img src=\"").concat(element.image, "\"/>\n    <section>\n    <h4>").concat(element.name, "</h4>\n    <ul>\n    <li>Prix petit format : 5.50 \u20AC</li>\n    <li>Prix grand format : 7.50 \u20AC</li>\n    </ul>\n    </section>\n    </a>\n    </article>");
});
console.log(html);
document.querySelector('.pageContent').innerHTML = html;
//# sourceMappingURL=main.js.map